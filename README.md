# php-common

Base image and default configuration for other php images.

## Environment variables

### PHP_DATE_TIMEZONE

Default: Europe/Berlin

### PHP_INPUT_NESTING_LIMIT

Default: 16

### PHP_INPUT_TIME_LIMIT

Default: 30 (seconds)

### PHP_MEMORY_LIMIT

Default: 64M

### PHP_POST_SIZE_LIMIT

Default: 8M

### PHP_SOCKET_TIMEOUT

Default: 30 (seconds)

### PHP_TIME_LIMIT

Default: 30 (seconds)

### PHP_UPLOAD_FILES_LIMIT

Default: 4

### PHP_UPLOAD_SIZE_LIMIT

Default: 8M
